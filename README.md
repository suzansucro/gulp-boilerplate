For production code, use files in **dist/**

For dev:
  - Clone & cd into repo
  - Install dependencies: `npm install -d`
  - Start gulp: `gulp`
  - Edit files in **app/** (except files in app/css/)
  - To build dist: `gulp build`
