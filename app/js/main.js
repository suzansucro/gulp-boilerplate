// <-- global data -->
var activeDataPoint = {
  name: '',
  index: ''
};

var areSynced = false;

var data_groups = {
  'Facebook': 'Facebook2',
  'Facebook2': 'Facebook',
  'Snapchat': 'Snapchat2',
  'Snapchat2': 'Snapchat',
  'Twitter': 'Twitter2',
  'Twitter2': 'Twitter'
}

// make data point active
// $(mything).animate({r: 4.375},2000)
// mything = $('.c3-circles-Snapchat circle')[0]

// <-- global functs -->
function syncDataPoints() {
  var synced = data_groups[activeDataPoint.name];
  var index = activeDataPoint.index;
  var syncedCircle = $(`.c3-circles-${synced} circle`)[index];
  $(syncedCircle).css('r', 5);
  areSynced = true;
}

function unsyncDataPoints() {
  var unsynced = data_groups[activeDataPoint.name];
  var index = activeDataPoint.index;
  var unsyncedCircle = $(`.c3-circles-${unsynced} circle`)[index];
  $(unsyncedCircle).css('r', 2.5);
  areSynced = false;
}


var chart = c3.generate({
  data: {
      x: 'x',
//        xFormat: '%Y', // 'xFormat' can be used as custom format of 'x'
      columns: [
          ['x', '2013-01-01', '2014-01-02', '2015-01-03', '2016-01-04', '2017-01-05', '2018-01-06'],
          ['Facebook', 30, 200, 100, 400, 150, 250],
          ['Facebook2', 30, 200, 100, 400, 140, 250],
          ['Twitter', 130, 340, 200, 500, 250, 350]
      ],
      onmouseover: function(d) {
        console.log('Mouse over');
        activeDataPoint = { name: '', index: '' };
        $(window).on('mousemove', function(e) {
          var tooltipIsDisplayed = $('.c3-tooltip-container').is(':visible');
          newDataPoint = $('.c3-tooltip-container td.name').text();
          if(newDataPoint !== activeDataPoint.name) {
            activeDataPoint = {
              name: newDataPoint,
              index: d.index
            }
            syncDataPoints();
            console.log(activeDataPoint);
          }
          if(tooltipIsDisplayed && !areSynced) {
            syncDataPoints();
          } else if (!tooltipIsDisplayed && areSynced) {
            unsyncDataPoints();
          }
        });
      },
      onmouseout: function(d) {
        console.log('Mouse out');
        $(window).off('mousemove');
      }
  },
  axis: {
      x: {
          type: 'timeseries',
          tick: {
              format: '%Y'
          }
      }
  },
  tooltip: {
    grouped: false
  },
  zoom: {
    enabled: true
  }
});

setTimeout(function () {
  chart.load({
      columns: [
          ['Snapchat', 400, 500, 450, 700, 600, 500]
      ]
  });
}, 1000);